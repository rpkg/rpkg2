.. rpkg documentation master file, created by
   sphinx-quickstart on Tue Dec 26 21:06:01 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to rpkg's documentation!
================================

.. toctree::
   :maxdepth: 2

   intro
   configuration
   cli
   api
   migrations
   releases

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
