.. _release-notes-1.48:

rpkg 1.48 Release Notes
=======================

Released on *December 22, 2016*

Python compatibility
--------------------

rpkg works with Python 2.6, 2.7 and 3.6.

Change Logs
-----------

- Better message when fail to authenticate via Kerberos - `#180`_ (cqi)

.. _`#180`: https://pagure.io/rpkg/issue/180
