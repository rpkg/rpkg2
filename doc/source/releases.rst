.. _releases:

Releases
========

.. toctree::
   :maxdepth: 1

   releases/1.56
   releases/1.55
   releases/1.54
   releases/1.53
   releases/1.52
   releases/1.51
   releases/1.50
   releases/1.49
   releases/1.48
   releases/1.47
