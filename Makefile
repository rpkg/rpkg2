ifdef IGNORE_PY26
	default_targets=detox
else
	default_targets=py26-test detox
endif

test: $(default_targets)
.PHONY: test

py26-test:
	@virtualenv --python=python2.6 .py26env
	@.py26env/bin/pip install "tox<3.0.0" "py<1.5.0" "virtualenv<16.0.0"
	@.py26env/bin/tox -e py26 -- $(TOX_POSARGS)
.PHONY: py26-test

detox:
	@detox -e py27,py36,py37,flake8 -- $(TOX_POSARGS)
.PHONY: detox

tox:
	@python3 -m venv .env
	@.env/bin/pip install tox
	@.env/bin/tox -e py27,py36,py37,flake8 -- $(TOX_POSARGS)
.PHONY: tox
