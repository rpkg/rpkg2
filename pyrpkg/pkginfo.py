# pyrpkg - a Python library for RPM Packagers
#
# Copyright (C) 2018 Red Hat Inc.
# Author(s): Chenxiong Qi <cqi@redhat.com>
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.  See http://www.gnu.org/copyleft/gpl.html for
# the full text of the license.

import logging
import os
import re
import subprocess
import posixpath

from six.moves import urllib
from pyrpkg.errors import rpkgError

logger = logging.getLogger(__name__)


class PackageMetadata(object):
    """Package metadata

    :param repo: object of
        :class:`PackageRepository <pyrpkg.pkgrepo.PackageRepository>`
        representing underlying git repository.
    :type repo: :class:`PackageRepository <pyrpkg.pkgrepo.PackageRepository>`
    :param bool distgit_namespaced: namespace is enabled in dist-git or not.
    """

    def __init__(self, repo, overwrite_target=None, distgit_namespaced=True):
        self.repo = repo
        self.distgit_namespaced = distgit_namespaced

        self._disttag = None
        self._distval = None
        self._distvar = None
        self._epoch = None
        self._localarch = None
        self._mockconfig = None
        self._module_name = None
        self._nvr = None
        self._spec = None
        self._rpmdefines = None
        self._ver = None
        self._ns = None

        # The build target for containers within the buildsystem
        self._container_build_target = overwrite_target
        self._target = overwrite_target

        # Deprecates self.module_name
        self._repo_name = None

    @property
    def disttag(self):
        """This property ensures the disttag attribute"""

        if not self._disttag:
            self.load_rpmdefines()
        return self._disttag

    @property
    def distval(self):
        """This property ensures the distval attribute"""

        if not self._distval:
            self.load_rpmdefines()
        return self._distval

    @property
    def distvar(self):
        """This property ensures the distvar attribute"""

        if not self._distvar:
            self.load_rpmdefines()
        return self._distvar

    @property
    def epoch(self):
        """This property ensures the epoch attribute"""

        if not self._epoch:
            self.load_nameverrel()
        return self._epoch

    @property
    def localarch(self):
        """This property ensures the module attribute"""

        if not self._localarch:
            self.load_localarch()
        return(self._localarch)

    def load_localarch(self):
        """Get the local arch as defined by rpm"""

        proc = subprocess.Popen(['rpm --eval %{_arch}'], shell=True,
                                stdout=subprocess.PIPE,
                                universal_newlines=True)
        self._localarch = proc.communicate()[0].strip('\n')

    @property
    def mockconfig(self):
        """This property ensures the mockconfig attribute"""

        if not self._mockconfig:
            self.load_mockconfig()
        return self._mockconfig

    @mockconfig.setter
    def mockconfig(self, config):
        self._mockconfig = config

    def load_mockconfig(self):
        """This sets the mockconfig attribute"""

        self._mockconfig = '%s-%s' % (self.target, self.localarch)

    @property
    def ns(self):
        """This property provides the namespace of the module"""

        if not self._ns:
            self.load_ns()
        return self._ns

    @ns.setter
    def ns(self, ns):
        self._ns = ns

    def load_ns(self):
        """Loads the namespace"""

        try:
            if self.distgit_namespaced:
                if self.repo.push_url:
                    parts = urllib.parse.urlparse(self.repo.push_url)

                    path_parts = [p for p in parts.path.split("/") if p]
                    if len(path_parts) == 1:
                        path_parts.insert(0, "rpms")
                    ns = path_parts[-2]

                    self._ns = ns
            else:
                self._ns = None
                logger.info("Could not find ns, distgit is not namespaced")
        except rpkgError:
            logger.warning('Failed to get ns from Git url or pushurl')

    @property
    def ns_module_name(self):
        logger.warning('Property ns_module_name is deprecated. '
                       'Please use ns_repo_name instead.')
        return self.ns_repo_name

    @property
    def ns_repo_name(self):
        if self.distgit_namespaced:
            return '%s/%s' % (self.ns, self.repo_name)
        else:
            return self.repo_name

    @property
    def nvr(self):
        """This property ensures the nvr attribute"""

        if not self._nvr:
            self.load_nvr()
        return self._nvr

    def load_nvr(self):
        """This sets the nvr attribute"""
        self._nvr = '{0}-{1}-{2}'.format(self.repo_name, self.ver, self.rel)

    @property
    def rel(self):
        """This property ensures the rel attribute"""
        if not self._rel:
            self.load_nameverrel()
        return(self._rel)

    def load_nameverrel(self):
        """Set the release of a package module."""

        cmd = ['rpm']
        cmd.extend(self.rpmdefines)
        # We make sure there is a space at the end of our query so that
        # we can split it later.  When there are subpackages, we get a
        # listing for each subpackage.  We only care about the first.
        cmd.extend(['-q', '--qf', '"%{NAME} %{EPOCH} %{VERSION} %{RELEASE}??"',
                    '--specfile', '"%s"' % os.path.join(self.repo.path, self.spec)])
        joined_cmd = ' '.join(cmd)
        try:
            proc = subprocess.Popen(joined_cmd, shell=True,
                                    universal_newlines=True,
                                    stdout=subprocess.PIPE,
                                    stderr=subprocess.PIPE)
            output, err = proc.communicate()
        except Exception as e:
            if err:
                logger.debug('Errors occoured while running following command to get N-V-R-E:')
                logger.debug(joined_cmd)
                logger.error(err)
            raise rpkgError('Could not query n-v-r of %s: %s'
                            % (self.module_name, e))
        if err:
            logger.debug('Errors occoured while running following command to get N-V-R-E:')
            logger.debug(joined_cmd)
            logger.error(err)
        # Get just the output, then split it by ??, grab the first and split
        # again to get ver and rel
        first_line_output = output.split('??')[0]
        parts = first_line_output.split()
        if len(parts) != 4:
            raise rpkgError('Could not get n-v-r-e from %r'
                            % first_line_output)
        (self._package_name_spec,
         self._epoch,
         self._ver,
         self._rel) = parts

        # Most packages don't include a "Epoch: 0" line, in which case RPM
        # returns '(none)'
        if self._epoch == "(none)":
            self._epoch = "0"

    @property
    def rpmdefines(self):
        """This property ensures the rpm defines"""

        if not self._rpmdefines:
            self.load_rpmdefines()
        return(self._rpmdefines)

    def load_rpmdefines(self):
        """Populate rpmdefines based on current active branch"""

        # This is another function ripe for subclassing

        try:
            # This regex should find the 'rhel-5' or 'rhel-6.2' parts of the
            # branch name.  There should only be one of those, and all branches
            # should end in one.
            osver = re.search(r'rhel-\d.*$', self.repo.branch_merge).group()
        except AttributeError:
            raise rpkgError('Could not find the base OS ver from branch name'
                            ' %s. Consider using --release option' %
                            self.repo.branch_merge)
        self._distvar, self._distval = osver.split('-')
        self._distval = self._distval.replace('.', '_')
        self._disttag = 'el%s' % self._distval
        self._rpmdefines = [
            "--define '_sourcedir %s'" % self.repo.path,
            "--define '_specdir %s'" % self.repo.path,
            "--define '_builddir %s'" % self.repo.path,
            "--define '_srcrpmdir %s'" % self.repo.path,
            "--define '_rpmdir %s'" % self.repo.path,
            "--define 'dist .%s'" % self._disttag,
            "--define '%s %s'" % (self._distvar, self._distval.split('_')[0]),
            # int and float this to remove the decimal
            "--define '%s 1'" % self._disttag
        ]

    @property
    def spec(self):
        """This property ensures the module attribute"""

        if not self._spec:
            self.load_spec()
        return self._spec

    def load_spec(self):
        """This sets the spec attribute"""

        deadpackage = False

        # Get a list of files in the path we're looking at
        files = os.listdir(self.repo.path)
        # Search the files for the first one that ends with ".spec"
        for f in files:
            if f.endswith('.spec') and not f.startswith('.'):
                self._spec = f
                return
            if f == 'dead.package':
                deadpackage = True
        if deadpackage:
            raise rpkgError('No spec file found. This package is retired')
        else:
            raise rpkgError('No spec file found.')

    @property
    def target(self):
        """This property ensures the target attribute"""

        if not self._target:
            self.load_target()
        return self._target

    def load_target(self):
        """This creates the target attribute based on branch merge"""

        # If a site has a different naming scheme, this would be where
        # a site would override
        self._target = '{0}-candidate'.format(self.repo.branch_merge)

    @property
    def container_build_target(self):
        """This property ensures the target for container builds."""
        if not self._container_build_target:
            self.load_container_build_target()
        return self._container_build_target

    def load_container_build_target(self):
        """This creates a target based on git branch and namespace."""
        self._container_build_target = '{0}-{1}-candidate'.format(
            self.repo.branch_merge, self.ns)

    @property
    def ver(self):
        """This property ensures the ver attribute"""
        if not self._ver:
            self.load_nameverrel()
        return(self._ver)

    @property
    def sources_filename(self):
        """File name of sources"""
        return os.path.join(self.repo.path, 'sources')

    @property
    def srpm_filename(self):
        return '{0}-{1}-{2}.src.rpm'.format(self.module_name, self.ver, self.rel)

    @property
    def repo_name(self):
        """Property to get repository name"""
        if not self._repo_name:
            self.load_repo_name()
        return self._repo_name

    @repo_name.setter
    def repo_name(self, name):
        """Set repository name"""
        self._repo_name = name

    @property
    def module_name(self):
        """This property ensures the module attribute"""
        logger.warning('Property module_name is deprecated. '
                       'Please use repo_name instead.')
        return self.repo_name

    @module_name.setter
    def module_name(self, module_name):
        logger.warning('Property module_name is deprecated. '
                       'Please use repo_name instead.')
        self.repo_name = module_name

    def load_module_name(self):
        return self.load_repo_name()

    def load_repo_name(self):
        """Loads repository name"""

        try:
            if self.repo.push_url:
                parts = urllib.parse.urlparse(self.repo.push_url)

                # FIXME
                # if self.distgit_namespaced:
                #     self._module_name = "/".join(parts.path.split("/")[-2:])
                repo_name = posixpath.basename(parts.path.strip('/'))

                if repo_name.endswith('.git'):
                    repo_name = repo_name[:-len('.git')]
                self._repo_name = repo_name
                return
        except rpkgError:
            logger.warning('Failed to get repository name from Git url or pushurl')

        self.load_nameverrel()
        if self._package_name_spec:
            self._repo_name = self._package_name_spec
            return

        raise rpkgError('Could not find current repository name.'
                        ' Use --name and optional --namespace.')
