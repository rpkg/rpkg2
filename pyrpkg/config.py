# config.py - interface to access configuration globally
#
# Copyright (C) 2018 Red Hat Inc.
# Author(s): Chenxiong Qi <cqi@redhat.com>
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.  See http://www.gnu.org/copyleft/gpl.html for
# the full text of the license.
#
# There are 6 functions derived from /usr/bin/koji which are licensed under
# LGPLv2.1.  See comments before those functions.

import os
import logging
import munch
import six

from pyrpkg import rpkgError

log = logging.getLogger('rpkg.config')

DEFAULT_CONFIG_FILENAME = 'rpkg.conf'
SYS_CONFIG_ROOT = '/etc/rpkg'
USER_CONFIG_ROOT = os.path.expanduser('~/.config/rpkg')

OPTIONAL_OPTIONS = {
    'distgit_namespaced': False,
    'distgit_namespaces': None,
    'kerberos_realms': '',
    'lookaside_namespaced': False,
    'lookaside_request_params': None,
}


class Munch(munch.Munch):
    """Customized to return default value of optional config options"""

    def has_option(self, option, section=None):
        """Check if there is option is defined

        This method is able to check an option in two ways:

        * When both section and option are specified, it checks option
          existence in form ``config.section.option``.
        * When option is specified only, just check in form ``config.option``.
          This is useful for checking core options defined in main application
          section.

        :param str option:
        :param str section:
        :return: True if option is defined. Otherwise, False is returned.
        :rtype: bool
        """
        if section:
            return section in self and option in self[section]
        else:
            return option in self

    def __getattr__(self, name):
        """
        Capture the last chance to return default value of optional options
        """
        try:
            return super(Munch, self).__getattr__(name)
        except AttributeError:
            if name in OPTIONAL_OPTIONS:
                return OPTIONAL_OPTIONS[name]
            else:
                raise


def get_option_value(config, section, option):
    try:
        return config.getboolean(section, option)
    except (ValueError,
            six.moves.configparser.InterpolationMissingOptionError):
        value = config.get(section, option, raw=True)
        if '\n' in value:
            return value.strip().split('\n')
        return value


def collect_config_files(profile_names):
    """Collect and return config files in correct order

    Refer to :meth:`read_config` to know how config files are collected.

    :param list profile_names: list of profile names based on which to collect
        config files.
    :return: list of config file names with absolute path. The file names under
        ``/etc/rpkg`` will be included first and then ``~/.config/rpkg`` if
        there are correspondingly.
    """
    sys_files = []
    user_files = []

    for profile in profile_names:
        sys_config = os.path.join(SYS_CONFIG_ROOT, '{0}.conf'.format(profile))
        if os.path.exists(sys_config):
            sys_files.append(sys_config)
        else:
            log.warning('Profile %s does not exist.', profile)

        user_config = os.path.join(USER_CONFIG_ROOT,
                                   '{0}.conf'.format(profile))
        if os.path.exists(user_config):
            user_files.append(user_config)
        else:
            log.debug('Profile %s does not exist for user config.', profile)

    return sys_files + user_files


def read_config(profiles=None, config_files=None):
    """Read configuration from config file

    Config files locates in directory ``/etc/rpkg/``.

    ``read_config`` reads config files by profile names, the found config files
    are read in the order of given profile names. Additional config files can
    be specified, which are read after reading profiles. Similarly, the order
    is also same as the order in which additional config files are given. As a
    result, additional config files could overwrite some options included in
    profiles.

    Profile name is mapped to the file name. For example, profile name is
    rpkg, whose config file is ``/etc/rpkg/rpkg.conf``, and profile name
    container-build, whose config file is ``/etc/rpkg/container-build.conf``,
    which is similar to user config files under ``~/.config/rpkg/``.

    If neither profile nor additional config file is given, it defaults to read
    ``/etc/rpkg/rpkg.conf``.

    The configuration file is an INI file containing core application options
    and its own subcommands options which are in separate corresponding
    sections.

    ``read_config`` reads all of those options into a Munch object, which
    allows to access options by dot calls, for example,
    ``config.rpkg.anongiturl``.

    Each value is read and converted to proper Python value. There are three
    supported types including boolean value, multiple lines string which is
    converted to a list of strings, and normal string.

    :param list profiles: list of profile names to collect config files.
    :param str config_files: alternative config files, which will be read after
        those specified via profiles.
    :return: a Munch object containing the core configuration options and
        options of all subsections as well.
    :rtype: :class:`Munch`.
    """
    if not profiles and not config_files:
        config_files = [os.path.join(SYS_CONFIG_ROOT, DEFAULT_CONFIG_FILENAME)]
    else:
        config_files = (
            (collect_config_files(profiles) if profiles else []) +
            (config_files or [])
        )

    config_loaded = False

    config = six.moves.configparser.ConfigParser()
    for filename in config_files:
        try:
            loaded_files = config.read(filename)
            if not loaded_files:
                log.warning('Cannot load config file %s.', filename)
            else:
                config_loaded = True
        except six.moves.configparser.MissingSectionHeaderError as e:
            raise rpkgError('Invalid config format in {0}\n{1}'.format(
                filename, str(e)))

    if not config_loaded:
        raise rpkgError('None of config files could be loaded.')

    result = Munch()

    def section_to_dict(section):
        return dict(
            (option, get_option_value(config, section, option))
            for option in config.options(section)
        )

    for section in config.sections():
        if '.' not in section:
            result[section] = Munch(section_to_dict(section))
        else:
            main_section, _, sub_section = section.partition('.')
            result[main_section][sub_section.replace('-', '_')] = Munch(
                section_to_dict(section))

    return result
