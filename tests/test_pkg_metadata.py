# -*- coding: utf-8 -*-

import os
import rpm
import shutil
import subprocess
import tempfile

from mock import patch, PropertyMock
from utils import CommandTestCase
from pyrpkg.errors import rpkgError
from pyrpkg.pkginfo import PackageMetadata
from pyrpkg.pkgrepo import PackageRepository


class TestProperties(CommandTestCase):
    """Test various package properties"""

    def setUp(self):
        super(TestProperties, self).setUp()
        self.repo = PackageRepository(self.cloned_repo_path)
        self.package = PackageMetadata(self.repo)
        self.invalid_repo = tempfile.mkdtemp()

    def tearDown(self):
        shutil.rmtree(self.invalid_repo)
        super(TestProperties, self).tearDown()

    def test_target(self):
        self.repo.git.checkout('eng-rhel-6')
        self.assertEqual('eng-rhel-6-candidate', self.package.target)

    def test_spec(self):
        self.assertEqual('docpkg.spec', self.package.spec)

    def test_no_spec_as_it_is_deadpackage(self):
        with patch('os.listdir', return_value=['dead.package']):
            self.assertRaises(rpkgError, self.package.load_spec)

    def test_no_spec_there(self):
        with patch('os.listdir', return_value=['anyfile']):
            self.assertRaises(rpkgError, self.package.load_spec)

    def test_nvr(self):
        repo = PackageRepository(self.cloned_repo_path,
                                 overwritten_branch_merge='eng-rhel-6')
        package = PackageMetadata(repo)

        module_name = os.path.basename(self.repo_path)
        self.assertEqual('{0}-1.2-2.el6'.format(module_name), package.nvr)

    def test_nvr_cannot_get_module_name_from_push_url(self):
        repo = PackageRepository(self.repo_path,
                                 overwritten_branch_merge='eng-rhel-6')
        package = PackageMetadata(repo)
        self.assertEqual('docpkg-1.2-2.el6', package.nvr)

    def test_localarch(self):
        expected_localarch = rpm.expandMacro('%{_arch}')
        self.assertEqual(expected_localarch, self.package.localarch)


class TestNamespacedModuleName(CommandTestCase):
    """Test PackageMetadata.ns_module_name"""

    def setUp(self):
        super(TestNamespacedModuleName, self).setUp()
        self.repo = PackageRepository(self.cloned_repo_path)

    def test_distgit_namespaced_is_disabled(self):
        tests = (
            ('http://localhost/rpms/docpkg.git', 'docpkg'),
            ('http://localhost/docker/docpkg.git', 'docpkg'),
            ('http://localhost/docpkg.git', 'docpkg'),
            ('http://localhost/rpms/docpkg', 'docpkg'),
            )
        for push_url, expected_ns_module_name in tests:
            with patch('pyrpkg.pkgrepo.PackageRepository.push_url',
                       new_callable=PropertyMock,
                       return_value=push_url):
                package = PackageMetadata(self.repo, distgit_namespaced=False)
                package.load_ns()
                package.load_module_name()
                self.assertEqual(
                    expected_ns_module_name, package.ns_module_name)

    def test_distgit_namedspaced_is_enabled(self):
        tests = (
            ('http://localhost/rpms/docpkg.git', 'rpms/docpkg'),
            ('http://localhost/docker/docpkg.git', 'docker/docpkg'),
            ('http://localhost/docpkg.git', 'rpms/docpkg'),
            ('http://localhost/rpms/docpkg', 'rpms/docpkg'),
            )
        for push_url, expected_ns_module_name in tests:
            with patch('pyrpkg.pkgrepo.PackageRepository.push_url',
                       new_callable=PropertyMock,
                       return_value=push_url):
                package = PackageMetadata(self.repo, distgit_namespaced=True)
                package.load_ns()
                package.load_module_name()
                self.assertEqual(
                    expected_ns_module_name, package.ns_module_name)


class LoadNameVerRelTest(CommandTestCase):
    """Test case for Commands.load_nameverrel"""

    def setUp(self):
        super(LoadNameVerRelTest, self).setUp()
        self.repo = PackageRepository(self.cloned_repo_path)
        self.package = PackageMetadata(self.repo)
        self.repo.git.checkout('eng-rhel-6')
        self.tempdir = tempfile.mkdtemp(prefix='rpkg_test_')

    def tearDown(self):
        super(LoadNameVerRelTest, self).tearDown()
        shutil.rmtree(self.tempdir)

    def test_load_from_spec(self):
        """Ensure name, version, release can be loaded from a valid SPEC"""
        self.package.load_nameverrel()
        self.assertEqual('docpkg', self.package._package_name_spec)
        self.assertEqual('0', self.package._epoch)
        self.assertEqual('1.2', self.package._ver)
        self.assertEqual('2.el6', self.package._rel)

    def test_load_spec_where_path_contains_space(self):
        """Ensure load_nameverrel works with a repo whose path contains space

        This test aims to test the space appearing in path does not break rpm
        command execution.

        For this test purpose, firstly, original repo has to be cloned to a
        new place which has a name containing arbitrary spaces.
        """
        cloned_repo_dir = os.path.join(self.tempdir, 'rpkg test cloned repo')
        if os.path.exists(cloned_repo_dir):
            shutil.rmtree(cloned_repo_dir)
        cloned_repo = self.repo.repo.clone(cloned_repo_dir)

        # Switching to branch eng-rhel-6 explicitly is required by running this
        # on RHEL6/7 because an old version of git is available in the
        # repo.
        # The failure reason is, old version of git makes the master as the
        # active branch in cloned repository, whatever the current active
        # branch is in the remote repository.
        # As of fixing this, I ran test on Fedora 23 with git 2.5.5, and test
        # fails on RHEL7 with git 1.8.3.1
        cloned_repo.git.checkout('eng-rhel-6')

        package = PackageMetadata(PackageRepository(cloned_repo_dir))
        package.load_nameverrel()

        self.assertEqual('docpkg', package._package_name_spec)
        self.assertEqual('0', package._epoch)
        self.assertEqual('1.2', package._ver)
        self.assertEqual('2.el6', package._rel)

    @patch('pyrpkg.pkginfo.PackageMetadata.rpmdefines', new_callable=PropertyMock)
    @patch('pyrpkg.pkginfo.PackageMetadata.spec', new_callable=PropertyMock)
    def test_load_when_rpm_fails(self, spec, rpmdefines):
        """Ensure rpkgError is raised when rpm command fails

        Commands.load_spec is mocked to help generate an incorrect rpm command
        line to cause the error that this test expects.

        Test test does not care about what rpm defines are retrieved from
        repository, so setting an empty list to Commands._rpmdefines is safe
        and enough.
        """
        spec.return_value = 'unknown-rpm-option a-nonexistent-package.spec'
        # For this test, no need of any rpmdefines
        rpmdefines.return_value = []
        self.assertRaises(rpkgError, self.package.load_nameverrel)


class LoadRPMDefinesTest(CommandTestCase):
    """Test case for Commands.load_rpmdefines"""

    def setUp(self):
        super(LoadRPMDefinesTest, self).setUp()
        self.package = PackageMetadata(
            PackageRepository(self.cloned_repo_path))

    def assert_loaded_rpmdefines(self, branch_name, expected_defines):
        self.package.repo.git.checkout(branch_name)

        self.package.load_rpmdefines()
        self.assertTrue(self.package._rpmdefines)

        # Convert defines into dict for assertion conveniently. The dict
        # contains mapping from variable name to value. For example,
        # {
        #     '_sourcedir': '/path/to/src-dir',
        #     '_specdir': '/path/to/spec',
        #     '_builddir': '/path/to/build-dir',
        #     '_srcrpmdir': '/path/to/srcrpm-dir',
        #     'dist': 'el7'
        # }
        defines = dict([
            item.split(' ')
            for item in (define.replace("'", '').split(' ', 1)[1]
                         for define in self.package._rpmdefines)
        ])

        for var, val in expected_defines.items():
            self.assertTrue(var in defines)
            self.assertEqual(val, defines[var])

    def test_load_rpmdefines_from_eng_rhel_6(self):
        """Run load_rpmdefines against branch eng-rhel-6"""
        expected_rpmdefines = {
            '_sourcedir': self.cloned_repo_path,
            '_specdir': self.cloned_repo_path,
            '_builddir': self.cloned_repo_path,
            '_srcrpmdir': self.cloned_repo_path,
            '_rpmdir': self.cloned_repo_path,
            'dist': u'.el6',
            'rhel': u'6',
            'el6': u'1',
            }
        self.assert_loaded_rpmdefines('eng-rhel-6', expected_rpmdefines)

    def test_load_rpmdefines_from_eng_rhel_6_5(self):
        """Run load_rpmdefines against branch eng-rhel-6.5

        Working on a different branch name is the only difference from test
        method test_load_rpmdefines_from_eng_rhel_6.
        """
        expected_rpmdefines = {
            '_sourcedir': self.cloned_repo_path,
            '_specdir': self.cloned_repo_path,
            '_builddir': self.cloned_repo_path,
            '_srcrpmdir': self.cloned_repo_path,
            '_rpmdir': self.cloned_repo_path,
            'dist': u'.el6_5',
            'rhel': u'6',
            'el6_5': u'1',
            }
        self.assert_loaded_rpmdefines('eng-rhel-6.5', expected_rpmdefines)

    @patch('pyrpkg.pkgrepo.PackageRepository.branch_merge',
           new_callable=PropertyMock)
    def test_load_rpmdefines_against_invalid_branch(self, branch_merge):
        branch_merge.return_value = 'invalid-branch-name'
        self.assertRaises(rpkgError, self.package.load_rpmdefines)


class TestLoadModuleNameFromSpecialPushURL(CommandTestCase):
    """Test load module name from a special push url that ends in /

    For issue: https://pagure.io/rpkg/issue/192
    """

    def setUp(self):
        super(TestLoadModuleNameFromSpecialPushURL, self).setUp()

        self.case_repo = tempfile.mkdtemp(prefix='case-test-load-module-name-')
        cmd = ['git', 'clone', '{0}/'.format(self.repo_path), self.case_repo]
        self.run_cmd(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

    def tearDown(self):
        shutil.rmtree(self.case_repo)
        super(TestLoadModuleNameFromSpecialPushURL, self).tearDown()

    def test_load_module_name(self):
        repo = PackageRepository(self.case_repo)
        package = PackageMetadata(repo)
        self.assertEqual(os.path.basename(self.repo_path), package.module_name)
