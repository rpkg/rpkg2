# -*- coding: utf-8 -*-

import errno
import io
import os
import shutil
import six
import subprocess
import tempfile

import git
from mock import call
from mock import patch
from mock import Mock
from mock import PropertyMock
from mock import mock_open

from pyrpkg import rpkgError

from utils import CommandTestCase


class CheckRepoWithOrWithoutDistOptionCase(CommandTestCase):
    """Check whether there are unpushed changes with or without specified dist

    Ensure check_repo works in a correct way to check if there are unpushed
    changes, and this should not be affected by specified dist or not.
    Bug 1169663 describes a concrete use case and this test case is designed
    as what that bug describs.
    """

    def setUp(self):
        super(CheckRepoWithOrWithoutDistOptionCase, self).setUp()

        private_branch = 'private-dev-branch'
        origin_repo = git.Repo(self.repo_path)
        origin_repo.git.checkout('master')
        origin_repo.git.branch(private_branch)
        self.make_a_dummy_commit(origin_repo)

        cloned_repo = git.Repo(self.cloned_repo_path)
        cloned_repo.git.pull()
        cloned_repo.git.checkout('-b', private_branch, 'origin/%s' % private_branch)
        for i in range(3):
            self.make_a_dummy_commit(cloned_repo)
        cloned_repo.git.push()

    def test_check_repo_with_specificed_dist(self):
        cmd = self.make_commands(self.cloned_repo_path, dist='eng-rhel-6')
        try:
            cmd.repo.check()
        except rpkgError as e:
            if 'There are unpushed changes in your repo' in e:
                self.fail('There are unpushed changes in your repo. This '
                          'should not happen. Something must be going wrong.')

            self.fail('Should not fail. Something must be going wrong.')

    def test_check_repo_without_specificed_dist(self):
        cmd = self.make_commands(self.cloned_repo_path)
        try:
            cmd.repo.check()
        except rpkgError as e:
            if 'There are unpushed changes in your repo' in e:
                self.fail('There are unpushed changes in your repo. This '
                          'should not happen. Something must be going wrong.')

            self.fail('Should not fail. Something must be going wrong.')


class ClogTest(CommandTestCase):

    create_repo_per_test = False

    def setUp(self):
        super(ClogTest, self).setUp()

        with open(os.path.join(self.cloned_repo_path, self.spec_file), 'w') as specfile:
            specfile.write('''
Summary: package demo
Name: pkgtool
Version: 0.1
Release: 1%{?dist}
License: GPL
%description
package demo for testing
%changelog
* Mon Nov 07 2016 tester@example.com
- add %%changelog section
- add new spec
$what_is_this

* Mon Nov 06 2016 tester@example.com
- initial
''')

        self.clog_file = os.path.join(self.cloned_repo_path, 'clog')
        self.cmd = self.make_commands()
        self.checkout_branch(self.cmd.repo, 'eng-rhel-6')

    def test_clog(self):
        self.cmd.clog()

        with open(self.clog_file, 'r') as clog:
            clog_lines = clog.readlines()

        expected_lines = ['add %changelog section\n',
                          'add new spec\n']
        self.assertEqual(expected_lines, clog_lines)

    def test_raw_clog(self):
        self.cmd.clog(raw=True)

        with open(self.clog_file, 'r') as clog:
            clog_lines = clog.readlines()

        expected_lines = ['- add %changelog section\n',
                          '- add new spec\n']
        self.assertEqual(expected_lines, clog_lines)


class TestNamespaced(CommandTestCase):

    require_test_repos = False

    def test_get_namespace_giturl(self):
        cmd = self.make_commands(path='/path/to/repo')
        cmd.gitbaseurl = 'ssh://%(user)s@localhost/%(module)s'
        cmd.distgit_namespaced = False

        self.assertEqual(cmd.gitbaseurl % {'user': cmd.user, 'module': 'docpkg'},
                         cmd._get_namespace_giturl('docpkg'))
        self.assertEqual(cmd.gitbaseurl % {'user': cmd.user, 'module': 'docker/docpkg'},
                         cmd._get_namespace_giturl('docker/docpkg'))
        self.assertEqual(cmd.gitbaseurl % {'user': cmd.user, 'module': 'rpms/docpkg'},
                         cmd._get_namespace_giturl('rpms/docpkg'))

    def test_get_namespace_giturl_namespaced_is_enabled(self):
        cmd = self.make_commands(path='/path/to/repo')
        cmd.gitbaseurl = 'ssh://%(user)s@localhost/%(module)s'
        cmd.distgit_namespaced = True

        self.assertEqual(cmd.gitbaseurl % {'user': cmd.user, 'module': 'rpms/docpkg'},
                         cmd._get_namespace_giturl('docpkg'))
        self.assertEqual(cmd.gitbaseurl % {'user': cmd.user, 'module': 'docker/docpkg'},
                         cmd._get_namespace_giturl('docker/docpkg'))
        self.assertEqual(cmd.gitbaseurl % {'user': cmd.user, 'module': 'rpms/docpkg'},
                         cmd._get_namespace_giturl('rpms/docpkg'))

    def test_get_namespace_anongiturl(self):
        cmd = self.make_commands(path='/path/to/repo')
        cmd.anongiturl = 'git://localhost/%(module)s'
        cmd.distgit_namespaced = False

        self.assertEqual(cmd.anongiturl % {'module': 'docpkg'},
                         cmd._get_namespace_anongiturl('docpkg'))
        self.assertEqual(cmd.anongiturl % {'module': 'docker/docpkg'},
                         cmd._get_namespace_anongiturl('docker/docpkg'))
        self.assertEqual(cmd.anongiturl % {'module': 'rpms/docpkg'},
                         cmd._get_namespace_anongiturl('rpms/docpkg'))

    def test_get_namespace_anongiturl_namespaced_is_enabled(self):
        cmd = self.make_commands(path='/path/to/repo')
        cmd.anongiturl = 'git://localhost/%(module)s'
        cmd.distgit_namespaced = True

        self.assertEqual(cmd.anongiturl % {'module': 'rpms/docpkg'},
                         cmd._get_namespace_anongiturl('docpkg'))
        self.assertEqual(cmd.anongiturl % {'module': 'docker/docpkg'},
                         cmd._get_namespace_anongiturl('docker/docpkg'))
        self.assertEqual(cmd.anongiturl % {'module': 'rpms/docpkg'},
                         cmd._get_namespace_anongiturl('rpms/docpkg'))


class TestGetLatestCommit(CommandTestCase):

    def test_get_latest_commit(self):
        cmd = self.make_commands(path=self.cloned_repo_path)
        # Repos used for running tests locates in local filesyste, refer to
        # self.repo_path and self.cloned_repo_path.
        cmd.anongiturl = '/tmp/%(module)s'
        cmd.distgit_namespaced = False

        self.assertEqual(str(six.next(git.Repo(self.repo_path).iter_commits())),
                         cmd.get_latest_commit(os.path.basename(self.repo_path),
                                               'eng-rhel-6'))


def load_kojisession(self):
    self._kojisession = Mock()
    self._kojisession.getFullInheritance.return_value = [
        {'child_id': 342, 'currdepth': 1, 'filter': [], 'intransitive': False,
         'maxdepth': None, 'name': 'f25-override', 'nextdepth': None, 'noconfig': False,
         'parent_id': 341, 'pkg_filter': '', 'priority': 0},
        {'child_id': 341, 'currdepth': 2, 'filter': [], 'intransitive': False,
         'maxdepth': None, 'name': 'f25-updates', 'nextdepth': None, 'noconfig': False,
         'parent_id': 336, 'pkg_filter': '', 'priority': 0},
        {'child_id': 336, 'currdepth': 3, 'filter': [], 'intransitive': False,
         'maxdepth': None, 'name': 'f25', 'nextdepth': None, 'noconfig': False,
         'parent_id': 335, 'pkg_filter': '', 'priority': 0},
        ]


class TestTagInheritanceTag(CommandTestCase):

    @patch('pyrpkg.Commands.load_kojisession', new=load_kojisession)
    def test_error_if_not_inherit(self):
        build_target = {
            'build_tag': 342, 'build_tag_name': 'f25-build',
            'dest_tag': 337, 'dest_tag_name': 'f25-updates-candidate',
            'id': 167, 'name': 'f25-candidate',
            }
        dest_tag = {
            'arches': None, 'extra': {},
            'id': 337, 'locked': False,
            'maven_include_all': False, 'maven_support': False,
            'name': 'f25-updates-candidate',
            'perm': None, 'perm_id': None,
            }

        cmd = self.make_commands()
        self.assertRaises(rpkgError, cmd.check_inheritance, build_target, dest_tag)


class TestLoginKojiSession(CommandTestCase):
    """Test login_koji_session"""

    require_test_repos = False

    def setUp(self):
        super(TestLoginKojiSession, self).setUp()

        self.cmd = self.make_commands(path='/path/to/repo')
        self.cmd.log = Mock()
        self.koji_config = {
            'authtype': 'ssl',
            'server': 'http://localhost/kojihub',
            'cert': '/path/to/cert',
            'ca': '/path/to/ca',
            'serverca': '/path/to/serverca',
        }
        self.session = Mock()

    @patch('pyrpkg.koji.is_requests_cert_error', return_value=True)
    def test_ssl_login_cert_revoked_or_expired(self, is_requests_cert_error):
        self.session.ssl_login.side_effect = Exception

        self.koji_config['authtype'] = 'ssl'

        self.assertRaises(rpkgError,
                          self.cmd.login_koji_session,
                          self.koji_config, self.session)
        self.cmd.log.info.assert_called_once_with(
            'Certificate is revoked or expired.')

    def test_ssl_login(self):
        self.koji_config['authtype'] = 'ssl'

        self.cmd.login_koji_session(self.koji_config, self.session)

        self.session.ssl_login.assert_called_once_with(
            self.koji_config['cert'],
            self.koji_config['ca'],
            self.koji_config['serverca'],
            proxyuser=None,
        )

    def test_runas_option_cannot_be_set_for_password_auth(self):
        self.koji_config['authtype'] = 'password'
        self.cmd.runas = 'user'
        self.assertRaises(rpkgError,
                          self.cmd.login_koji_session,
                          self.koji_config, self.session)

    @patch('pyrpkg.Commands.user', new_callable=PropertyMock)
    def test_password_login(self, user):
        user.return_value = 'tester'
        self.session.opts = {}
        self.koji_config['authtype'] = 'password'

        self.cmd.login_koji_session(self.koji_config, self.session)

        self.assertEqual({'user': 'tester', 'password': None},
                         self.session.opts)
        self.session.login.assert_called_once()

    @patch('pyrpkg.Commands._load_krb_user', return_value=False)
    def test_krb_login_fails_if_no_valid_credential(self, _load_krb_user):
        self.koji_config['authtype'] = 'kerberos'
        self.cmd.realms = ['FEDORAPROJECT.ORG']

        self.cmd.login_koji_session(self.koji_config, self.session)

        self.session.krb_login.assert_not_called()
        self.assertEqual(2, self.cmd.log.warning.call_count)

    @patch('pyrpkg.Commands._load_krb_user', return_value=True)
    def test_krb_login_fails(self, _load_krb_user):
        self.koji_config['authtype'] = 'kerberos'
        # Simulate ClientSession.krb_login fails and error is raised.
        self.session.krb_login.side_effect = Exception

        self.cmd.login_koji_session(self.koji_config, self.session)

        self.session.krb_login.assert_called_once_with(proxyuser=None)
        self.cmd.log.error.assert_called_once()

    @patch('pyrpkg.Commands._load_krb_user', return_value=True)
    def test_successful_krb_login(self, _load_krb_user):
        self.koji_config['authtype'] = 'kerberos'

        self.cmd.login_koji_session(self.koji_config, self.session)

        self.session.krb_login.assert_called_once_with(proxyuser=None)


class TestConstructBuildURL(CommandTestCase):
    """Test Commands.construct_build_url"""

    create_repo_per_test = False

    def setUp(self):
        super(TestConstructBuildURL, self).setUp()
        self.cmd = self.make_commands()

    @patch('pyrpkg.PackageMetadata.ns_repo_name', new_callable=PropertyMock)
    @patch('pyrpkg.PackageRepository.commit_hash', new_callable=PropertyMock)
    def test_construct_url(self, commit_hash, ns_repo_name):
        commit_hash.return_value = '12345'
        ns_repo_name.return_value = 'container/fedpkg'

        anongiturl = 'https://src.example.com/%(repo)s'
        with patch.object(self.cmd, 'anongiturl', new=anongiturl):
            url = self.cmd.construct_build_url()

        expected_url = '{0}#{1}'.format(
            anongiturl % {'repo': ns_repo_name.return_value},
            commit_hash.return_value)
        self.assertEqual(expected_url, url)

    def test_construct_with_given_repo_name_and_hash(self):
        anongiturl = 'https://src.example.com/%(repo)s'
        with patch.object(self.cmd, 'anongiturl', new=anongiturl):
            for repo_name in ('extra-cmake-modules', 'rpms/kf5-kfilemetadata'):
                url = self.cmd.construct_build_url(repo_name, '123456')

                expected_url = '{0}#{1}'.format(
                    anongiturl % {'repo': repo_name}, '123456')
                self.assertEqual(expected_url, url)


class TestCleanupTmpDir(CommandTestCase):
    """Test Commands._cleanup_tmp_dir for mockbuild command"""

    require_test_repos = False

    def setUp(self):
        super(TestCleanupTmpDir, self).setUp()
        self.tmp_dir_name = tempfile.mkdtemp(prefix='test-cleanup-tmp-dir-')
        self.cmd = self.make_commands(path='/path/to/repo')

    def tearDown(self):
        if os.path.exists(self.tmp_dir_name):
            os.rmdir(self.tmp_dir_name)
        super(TestCleanupTmpDir, self).tearDown()

    @patch('shutil.rmtree')
    def test_do_nothing_is_tmp_dir_is_invalid(self, rmtree):
        for invalid_dir in ('', None):
            self.cmd._cleanup_tmp_dir(invalid_dir)
            rmtree.assert_not_called()

    def test_remove_tmp_dir(self):
        self.cmd._cleanup_tmp_dir(self.tmp_dir_name)

        self.assertFalse(os.path.exists(self.tmp_dir_name))

    def test_keep_silient_if_tmp_dir_does_not_exist(self):
        tmp_dir = tempfile.mkdtemp()
        os.rmdir(tmp_dir)

        self.cmd._cleanup_tmp_dir(tmp_dir)

    def test_raise_error_if_other_non_no_such_file_dir_error(self):
        with patch('shutil.rmtree',
                   side_effect=OSError((errno.EEXIST), 'error message')):
            self.assertRaises(rpkgError, self.cmd._cleanup_tmp_dir, '/tmp/dir')


class TestConfigMockConfigDir(CommandTestCase):
    """Test Commands._config_dir_basic for mockbuild"""

    create_repo_per_test = False

    def setUp(self):
        super(TestConfigMockConfigDir, self).setUp()

        self.cmd = self.make_commands()
        self.temp_config_dir = tempfile.mkdtemp()

        self.fake_root = 'fedora-26-x86_64'
        self.mock_config_patcher = patch('pyrpkg.Commands.mock_config',
                                         return_value=u'mock config x86_64')
        self.mock_mock_config = self.mock_config_patcher.start()

        self.mkdtemp_patcher = patch('tempfile.mkdtemp',
                                     return_value=self.temp_config_dir)
        self.mock_mkdtemp = self.mkdtemp_patcher.start()

    def tearDown(self):
        shutil.rmtree(self.temp_config_dir)
        self.mkdtemp_patcher.stop()
        self.mock_config_patcher.stop()
        super(TestConfigMockConfigDir, self).tearDown()

    def test_config_in_created_config_dir(self):
        config_dir = self.cmd._config_dir_basic(root=self.fake_root)

        self.assertEqual(self.temp_config_dir, config_dir)

        config_file = '{0}.cfg'.format(
            os.path.join(self.temp_config_dir, self.fake_root))
        with io.open(config_file, 'r', encoding='utf-8') as f:
            self.assertEqual(self.mock_mock_config.return_value,
                             f.read().strip())

    def test_config_in_specified_config_dir(self):
        config_dir = self.cmd._config_dir_basic(
            config_dir=self.temp_config_dir,
            root=self.fake_root)

        self.assertEqual(self.temp_config_dir, config_dir)

        config_file = '{0}.cfg'.format(
            os.path.join(self.temp_config_dir, self.fake_root))
        with io.open(config_file, 'r', encoding='utf-8') as f:
            self.assertEqual(self.mock_mock_config.return_value,
                             f.read().strip())

    @patch('pyrpkg.pkginfo.PackageMetadata.mockconfig',
           new_callable=PropertyMock)
    def test_config_using_root_guessed_from_branch(self, mockconfig):
        mockconfig.return_value = 'f26-candidate-i686'

        config_dir = self.cmd._config_dir_basic()

        self.assertEqual(self.temp_config_dir, config_dir)

        config_file = '{0}.cfg'.format(
            os.path.join(self.temp_config_dir, mockconfig.return_value))
        with io.open(config_file, 'r', encoding='utf-8') as f:
            self.assertEqual(self.mock_mock_config.return_value,
                             f.read().strip())

    def test_fail_if_error_occurs_while_getting_mock_config(self):
        self.mock_mock_config.side_effect = rpkgError

        with patch('pyrpkg.Commands._cleanup_tmp_dir') as mock:
            self.assertRaises(
                rpkgError, self.cmd._config_dir_basic, root=self.fake_root)
            mock.assert_called_once_with(self.mock_mkdtemp.return_value)

        with patch('pyrpkg.Commands._cleanup_tmp_dir') as mock:
            self.assertRaises(rpkgError,
                              self.cmd._config_dir_basic,
                              config_dir='/path/to/fake/config-dir',
                              root=self.fake_root)
            mock.assert_called_once_with(None)

    def test_fail_if_error_occurs_while_writing_cfg_file(self):
        with patch.object(io, 'open') as m:
            m.return_value.__enter__.return_value.write.side_effect = IOError

            with patch('pyrpkg.Commands._cleanup_tmp_dir') as mock:
                self.assertRaises(rpkgError,
                                  self.cmd._config_dir_basic,
                                  root=self.fake_root)
                mock.assert_called_once_with(self.mock_mkdtemp.return_value)

            with patch('pyrpkg.Commands._cleanup_tmp_dir') as mock:
                self.assertRaises(rpkgError,
                                  self.cmd._config_dir_basic,
                                  config_dir='/path/to/fake/config-dir',
                                  root=self.fake_root)
                mock.assert_called_once_with(None)


class TestConfigMockConfigDirWithNecessaryFiles(CommandTestCase):
    """Test Commands._config_dir_other"""

    require_test_repos = False

    def setUp(self):
        super(TestConfigMockConfigDirWithNecessaryFiles, self).setUp()
        self.cmd = self.make_commands(path='/path/to/repo')

    @patch('shutil.copy2')
    @patch('os.path.exists', return_value=True)
    def test_copy_cfg_files_from_etc_mock_dir(self, exists, copy2):
        self.cmd._config_dir_other('/path/to/config-dir')

        exists.assert_has_calls([
            call('/etc/mock/site-defaults.cfg'),
            call('/etc/mock/logging.ini')
        ])
        copy2.assert_has_calls([
            call('/etc/mock/site-defaults.cfg',
                 '/path/to/config-dir/site-defaults.cfg'),
            call('/etc/mock/logging.ini',
                 '/path/to/config-dir/logging.ini'),
        ])

    @patch('os.path.exists', return_value=False)
    def test_create_empty_cfg_files_if_not_exist_in_system_mock(self, exists):
        with patch.object(six.moves.builtins, 'open', mock_open()) as m:
            self.cmd._config_dir_other('/path/to/config-dir')

            m.assert_has_calls([
                call('/path/to/config-dir/site-defaults.cfg', 'w'),
                call().close(),
                call('/path/to/config-dir/logging.ini', 'w'),
                call().close(),
            ])

        exists.assert_has_calls([
            call('/etc/mock/site-defaults.cfg'),
            call('/etc/mock/logging.ini')
        ])

    @patch('shutil.copy2')
    @patch('os.path.exists', return_value=True)
    def test_fail_when_copy_cfg_file(self, exists, copy2):
        copy2.side_effect = OSError

        self.assertRaises(
            rpkgError, self.cmd._config_dir_other, '/path/to/config-dir')

    @patch('os.path.exists', return_value=False)
    def test_fail_if_error_when_write_empty_cfg_files(self, exists):
        with patch.object(six.moves.builtins, 'open', mock_open()) as m:
            m.side_effect = IOError
            self.assertRaises(
                rpkgError, self.cmd._config_dir_other, '/path/to/config-dir')


class TestLint(CommandTestCase):

    @patch('glob.glob')
    @patch('os.path.exists')
    @patch('pyrpkg.Commands._run_command')
    @patch('pyrpkg.PackageMetadata.repo_name', new_callable=PropertyMock)
    @patch('pyrpkg.PackageMetadata.rpmdefines', new_callable=PropertyMock)
    @patch('pyrpkg.PackageMetadata.rel', new_callable=PropertyMock)
    def test_lint_each_file_once(
            self, rel, rpmdefines, module_name, run, exists, glob):
        rel.return_value = '2.fc26'
        rpmdefines.return_value = []
        module_name.return_value = 'docpkg'

        cmd = self.make_commands()
        srpm_path = os.path.join(cmd.path, 'docpkg-1.2-2.fc26.src.rpm')
        bin_path = os.path.join(cmd.path, 'x86_64', 'docpkg-1.2-2.fc26.x86_64.rpm')

        def _mock_exists(path):
            return path in [
                srpm_path,
                os.path.join(cmd.path, 'x86_64'),

                # For checking existence of repo directory when visit property
                # self.repo
                self.cloned_repo_path,
            ]

        def _mock_glob(g):
            return {
                os.path.join(cmd.path, 'x86_64', '*.rpm'): [bin_path],
            }[g]
        exists.side_effect = _mock_exists
        glob.side_effect = _mock_glob
        cmd._get_build_arches_from_spec = Mock(return_value=['x86_64', 'x86_64'])

        cmd.lint()

        self.assertEqual(
            run.call_args_list,
            [call(['rpmlint',
                   os.path.join(cmd.path, 'docpkg.spec'),
                   srpm_path,
                   bin_path,
                   ], shell=True)])


class TestSpecifyTarget(CommandTestCase):
    """
    Test build target is correct with specified target rather than guessing
    from release branch
    """

    def test_specifiy_custom_target_for_normal_package_build(self):
        expected_target = 'new-release-candidate'
        cmd = self.make_commands(target=expected_target)
        self.assertEqual(expected_target, cmd.package.target)

    def test_specified_custom_target_for_container_build(self):
        expected_target = 'container-candidate'
        cmd = self.make_commands(target=expected_target)
        self.assertEqual(expected_target, cmd.package.container_build_target)


class TestRunCommand(CommandTestCase):
    """Test _run_command"""

    require_test_repos = False

    def setUp(self):
        super(TestRunCommand, self).setUp()
        self.cmd = self.make_commands(path='/path/to/repo')

    @patch('subprocess.Popen')
    def test_run_command_within_shell(self, Popen):
        Popen.return_value.wait.return_value = 0

        result = self.cmd._run_command(['rpmbuild'], shell=True)

        self.assertEqual((0, None, None), result)
        Popen.assert_called_once_with(
            'rpmbuild', env=os.environ, shell=True, cwd=None,
            stdout=None, stderr=None, universal_newlines=False)

    @patch('subprocess.Popen')
    def test_run_command_without_shell(self, Popen):
        Popen.return_value.wait.return_value = 0

        result = self.cmd._run_command(['rpmbuild'])

        self.assertEqual((0, None, None), result)
        Popen.assert_called_once_with(
            ['rpmbuild'], env=os.environ, shell=False, cwd=None,
            stdout=None, stderr=None, universal_newlines=False)

    @patch('subprocess.Popen')
    def test_return_stdout(self, Popen):
        Popen.return_value.wait.return_value = 0
        Popen.return_value.stdout.read.return_value = 'output'

        result = self.cmd._run_command(
            ['rpmbuild'], shell=True, return_stdout=True)

        self.assertEqual((0, 'output', None), result)
        Popen.assert_called_once_with(
            'rpmbuild', env=os.environ, shell=True, cwd=None,
            stdout=subprocess.PIPE, stderr=None, universal_newlines=False)

    @patch('subprocess.Popen')
    def test_return_stderr(self, Popen):
        Popen.return_value.wait.return_value = 0
        Popen.return_value.stderr.read.return_value = 'output'

        result = self.cmd._run_command(
            ['rpmbuild'], shell=True, return_stderr=True)

        self.assertEqual((0, None, 'output'), result)
        Popen.assert_called_once_with(
            'rpmbuild', env=os.environ, shell=True, cwd=None,
            stdout=None, stderr=subprocess.PIPE, universal_newlines=False)

    @patch('subprocess.Popen')
    def test_pipe(self, Popen):
        Popen.return_value.wait.return_value = 0

        first_proc = Mock()
        second_proc = Mock()
        second_proc.wait.return_value = 0

        Popen.side_effect = [first_proc, second_proc]

        result = self.cmd._run_command(
            ['rpmbuild'], pipe=['grep', 'src.rpm'])

        self.assertEqual((0, None, None), result)
        Popen.assert_has_calls([
            call(['rpmbuild'],
                 env=os.environ, shell=False, cwd=None,
                 stdout=subprocess.PIPE, stderr=subprocess.STDOUT),
            call(['grep', 'src.rpm'],
                 env=os.environ, shell=False, cwd=None,
                 stdin=first_proc.stdout, stdout=None, stderr=None,
                 universal_newlines=False),
        ])

    @patch('subprocess.Popen')
    def test_raise_error_if_error_is_raised_from_subprocess(self, Popen):
        Popen.side_effect = OSError(1, "msg")

        six.assertRaisesRegex(
            self, rpkgError, 'msg', self.cmd._run_command, ['rpmbuild'])

    @patch('subprocess.Popen')
    def test_raise_error_if_command_returns_nonzeror(self, Popen):
        Popen.return_value.wait.return_value = 1
        Popen.return_value.stderr.read.return_value = ''

        six.assertRaisesRegex(
            self, rpkgError, 'Failed to execute command',
            self.cmd._run_command, ['rpmbuild'])

    @patch('subprocess.Popen')
    def test_return_error_msg_if_return_stderr_is_set(self, Popen):
        Popen.return_value.wait.return_value = 1
        Popen.return_value.stderr.read.return_value = 'something wrong'

        result = self.cmd._run_command(['rpmbuild'], return_stderr=True)
        self.assertEqual((1, None, 'something wrong'), result)

    @patch('subprocess.Popen')
    def test_set_envs(self, Popen):
        Popen.return_value.wait.return_value = 0

        with patch.dict('os.environ', {}, clear=True):
            result = self.cmd._run_command(['rpmbuild'], env={'myvar': 'test'})

            self.assertEqual((0, None, None), result)
            Popen.assert_called_once_with(
                ['rpmbuild'], env={'myvar': 'test'},
                shell=False, cwd=None, stdout=None, stderr=None,
                universal_newlines=False)

    @patch('subprocess.Popen')
    def test_run_command_in_a_directory(self, Popen):
        Popen.return_value.wait.return_value = 0

        self.cmd._run_command(['rpmbuild'], cwd='/tmp/builddir')

        Popen.assert_called_once_with(
            ['rpmbuild'], env=os.environ, shell=False, cwd='/tmp/builddir',
            stdout=None, stderr=None, universal_newlines=False)

    @patch('subprocess.Popen')
    def test_return_output_as_text(self, Popen):
        Popen.return_value.wait.return_value = 0

        self.cmd._run_command(
            ['rpmbuild'], return_stdout=True, return_text=True)

        Popen.assert_called_once_with(
            ['rpmbuild'], env=os.environ, shell=False, cwd=None,
            stdout=subprocess.PIPE, stderr=None, universal_newlines=True)
