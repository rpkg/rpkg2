# pyrpkg - a Python library for RPM Packagers
#
# Copyright (C) 2018 Red Hat Inc.
# Author(s): Chenxiong Qi <cqi@redhat.com>
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.  See http://www.gnu.org/copyleft/gpl.html for
# the full text of the license.

import os
import shutil
import six
import tempfile
import pyrpkg.config as rpkgconf

from mock import call, patch
from utils import unittest
from pyrpkg.errors import rpkgError

fixtures_dir = os.path.join(os.path.dirname(__file__), 'fixtures')
# rpkg.conf for running tests below
rpkg_conf = os.path.join(fixtures_dir, 'rpkg.conf')


class TestReadConfig(unittest.TestCase):
    """Test config.read_config"""

    def setUp(self):
        self.directories_to_delete = []

    def tearDown(self):
        for item in self.directories_to_delete:
            shutil.rmtree(item)

    def test_read_config(self):
        config = rpkgconf.read_config(config_files=[rpkg_conf])

        self.assertEqual('ssh://%(user)s@localhost/%(repo)s',
                         config.rpkg.gitbaseurl)
        self.assertEqual('mbs-authorizer', config.rpkg.mbs.oidc_client_id)
        self.assertTrue(isinstance(config.rpkg.bodhi.staging, bool))
        self.assertTrue(config.rpkg.bodhi.staging)
        self.assertEqual([
            'bz.default-component %(repo)s',
            'bz.default-tracker bugzilla.redhat.com',
            'bz.default-product Fedora',
        ], config.rpkg.clone_config)

    def test_read_config_from_multiple_config_files(self):
        fake_sys_config_root = tempfile.mkdtemp()
        fake_user_config_root = tempfile.mkdtemp()
        self.directories_to_delete.append(fake_sys_config_root)
        self.directories_to_delete.append(fake_user_config_root)

        config_files = (
            (os.path.join(fake_sys_config_root, 'rpkg.conf'),
             '[rpkg]\nanongiturl=http://localhost.distgit/'),
            (os.path.join(fake_sys_config_root, 'module.conf'),
             '[rpkg.mbs]\nurl=http://localhost.mbs/'),

            # Config file for profile module to overwrite and add new option
            (os.path.join(fake_user_config_root, 'module.conf'),
             '[rpkg.mbs]\nurl=http://test.mbs/\nstaging=True'),

            # A custom config file to overwrite nongiturl which points to another
            # server.
            (os.path.join(fake_user_config_root, 'test.conf'),
             '[rpkg]\n'
             'anongiturl=http://test.distgit/\n'
             'distgit_namespaced=True'),
        )

        with patch.object(rpkgconf, 'SYS_CONFIG_ROOT', fake_sys_config_root):
            with patch.object(rpkgconf, 'USER_CONFIG_ROOT',
                              fake_user_config_root):
                for filename, content in config_files:
                    with open(filename, 'w') as f:
                        f.write(content)

                config = rpkgconf.read_config(
                    profiles=['rpkg', 'module'],
                    config_files=[config_files[3][0]])

        self.assertEqual('http://test.distgit/', config.rpkg.anongiturl)
        self.assertTrue(config.rpkg.mbs.staging)
        self.assertEqual('http://test.mbs/', config.rpkg.mbs.url)
        self.assertTrue(config.rpkg.distgit_namespaced)

    def test_convert_section_name(self):
        config = rpkgconf.read_config(config_files=[rpkg_conf])
        self.assertEqual('koji', config.rpkg.image_build.kojiprofile)

    def test_raise_error_if_option_is_not_set(self):
        config = rpkgconf.read_config(config_files=[rpkg_conf])

        with self.assertRaises(AttributeError):
            config.rpkg.xxx

    def test_return_default_optional_option_value(self):
        config = rpkgconf.read_config(config_files=[rpkg_conf])
        # distgit_namespaced is not defined in fixtures/rpkg.conf
        self.assertFalse(config.rpkg.distgit_namespaced)

    @patch('six.moves.configparser.ConfigParser')
    def test_read_from_profiles(self, ConfigParser):
        config_parser = ConfigParser.return_value

        profiles = ['rpkg', 'container-build', 'module-build']
        expected_config_files = [
            '/etc/rpkg/{0}.conf'.format(item) for item in profiles
        ] + [
            os.path.expanduser('~/.config/rpkg/{0}.conf'.format(item))
            for item in profiles
        ]
        config_parser.read.return_value = expected_config_files

        with patch('os.path.exists', return_value=True):
            rpkgconf.read_config(profiles)

        config_parser.read.assert_has_calls([
            call(filename) for filename in expected_config_files
        ])

    @patch('six.moves.configparser.ConfigParser')
    def test_read_from_profiles_but_part_config_files_do_not_exist(
            self, ConfigParser):
        config_parser = ConfigParser.return_value

        profiles = ['rpkg', 'container-build', 'module-build']
        expected_config_files = [
            '/etc/rpkg/{0}.conf'.format(item) for item in profiles
        ]
        config_parser.read.return_value = expected_config_files

        def exists(path):
            return not path.startswith(os.path.expanduser('~/.config'))

        with patch('os.path.exists', side_effect=exists):
            rpkgconf.read_config(profiles)

        config_parser.read.assert_has_calls([
            call(filename) for filename in expected_config_files
        ])

    @patch('six.moves.configparser.ConfigParser')
    def test_raise_error_if_none_config_files_is_loaded(self, ConfigParser):
        config_parser = ConfigParser.return_value
        # Mock ConfigParser cannot load any file.
        config_parser.read.return_value = []

        fake_sys_config_root = tempfile.mkdtemp()
        self.directories_to_delete.append(fake_sys_config_root)
        with patch.object(rpkgconf, 'SYS_CONFIG_ROOT',
                          new=fake_sys_config_root):
            six.assertRaisesRegex(
                self, rpkgError, 'None of config files could be loaded',
                rpkgconf.read_config, profiles=['rpkg', 'container-build'])

    def test_raise_error_if_config_file_is_malformat(self):
        fake_sys_config_root = tempfile.mkdtemp()
        with open(os.path.join(fake_sys_config_root, 'rpkg.conf'), 'w') as f:
            f.write('nongiturl=http://localhost/')
        self.directories_to_delete.append(fake_sys_config_root)

        with patch.object(rpkgconf, 'SYS_CONFIG_ROOT',
                          new=fake_sys_config_root):
            six.assertRaisesRegex(
                self, rpkgError, 'Invalid config format',
                rpkgconf.read_config, profiles=['rpkg'])

    @patch('six.moves.configparser.ConfigParser')
    def test_read_from_profiles_and_alternative_config_files(
            self, ConfigParser):
        config_parser = ConfigParser.return_value

        profiles = ['rpkg', 'container-build', 'module-build']
        expected_config_files = [
            '/etc/rpkg/{0}.conf'.format(item) for item in profiles
        ] + [
            os.path.expanduser('~/.config/rpkg/{0}.conf'.format(item))
            for item in profiles
        ]
        config_parser.read.return_value = expected_config_files

        other_config_files = ['path/to/custom.conf']
        with patch('os.path.exists', return_value=True):
            rpkgconf.read_config(profiles, config_files=other_config_files)

        expected_calls = [
            call(filename) for filename in expected_config_files
        ] + [
            call(filename) for filename in other_config_files
        ]
        config_parser.read.assert_has_calls(expected_calls)

    def test_has_option_in_section(self):
        config = rpkgconf.read_config(config_files=[rpkg_conf]).rpkg
        self.assertTrue(config.has_option('staging', section='bodhi'))
        self.assertFalse(config.has_option('url', section='bodhi'))

    def test_has_option_in_current_section(self):
        config = rpkgconf.read_config(config_files=[rpkg_conf]).rpkg
        self.assertTrue(config.has_option('lookasidehash'))
        self.assertFalse(config.has_option('xxx'))

    @patch('six.moves.configparser.ConfigParser')
    def test_read_default_config(self, ConfigParser):
        config_parser = ConfigParser.return_value

        rpkgconf.read_config()
        config_parser.read.assert_called_once_with(
            os.path.join(rpkgconf.SYS_CONFIG_ROOT,
                         rpkgconf.DEFAULT_CONFIG_FILENAME))
